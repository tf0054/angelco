(ns angelco.core
  (:use [net.cgrand.enlive-html])
  (:require [org.httpkit.client :as http]
            [clojure.java.io :as io]
            [cheshire.core :refer :all]) 
  )

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World"))

(def options {:timeout 200             ; ms
              ;;:basic-auth ["user" "pass"]
              ;;:query-params {:param "value" :param2 ["value1" "value2"]}
              :user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"
              :headers {"Accept" "application/json, text/javascript, */*; q=0.01"
                        ;;"X-CSRF-Token" "VWtNBvjSj0zTCBlQeCSw+9v97LMUXWFS6uU+HuEndLg="
                        "Referer" "https://angel.co/berlin"
                        "Cookie" "ajs_anonymous_id=%22f14dd8da-f094-47aa-8605-098ec4b317ff%22; _ga=GA1.2.750917237.1489412644; ajs_group_id=null; ajs_user_id=%223910173%22; mp_6a8c8224f4f542ff59bd0e2312892d36_mixpanel=%7B%22distinct_id%22%3A%20%223910173%22%2C%22%24search_engine%22%3A%20%22google%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.google.com%2F%22%2C%22%24initial_referring_domain%22%3A%20%22www.google.com%22%2C%22%24username%22%3A%20%22tf0054%22%2C%22angel%22%3A%20false%2C%22candidate%22%3A%20false%2C%22roles%22%3A%20%5B%0A%20%20%20%20%22Research%20%26%20Development%22%0A%5D%2C%22quality_ceiling%22%3A%20%222%22%7D; _angellist=668e480d386dbed78bc2715996a15775; mp_mixpanel__c=15"
                        "X-Requested-With" "XMLHttpRequest"}})

(defn fetch-page
  [file-path]
  (html-resource (java.io.StringReader.
                  (:html (parse-string (slurp file-path) true))
                  )))

(defn extract-companies [page]
  (select page [:div.base]))

(defn _remove-attr [page x]
  (transform
   page   
   [:div]
   (remove-attr x)) )

(defn _ext-from-name [page x]
  (transform
   (select page [:div.name :a])
   [:a]
   #(-> (assoc % :tag :div)
        (assoc :content [(x (:attrs %))])
        )))

(defn extract-id [page]
  (-> (_ext-from-name page :data-id)
      (_remove-attr :data-id)
      (_remove-attr :data-type)
      (_remove-attr :href)
      (_remove-attr :class) )
  )

(defn extract-type [page]
  (-> (_ext-from-name page :data-type)
      (_remove-attr :data-id)
      (_remove-attr :data-type)
      (_remove-attr :href)
      (_remove-attr :class) )
  )

(defn extract-name [page]
  (select page [:div.name]))

(defn extract-joined [page]
  (select page [:div.joined :div.value]))

(defn extract-content [page]
  (select page [:div.blurb]))

(defn extract-tags [page]
  (select page [:div.tags
                ;;(nth-last-child 1)
                ]))

(defn extract-main-from-file [filename]
  (let [data-file (io/file
                   (io/resource filename))]
    (let [companies (->> data-file
                         fetch-page
                         extract-companies
                         )]
      (map (fn [x] {:tag :tr :content (transform
                                       (concat
                                        (extract-id x)
                                        (extract-type x)
                                        (extract-name x)
                                        (extract-joined x)
                                        (extract-content x)
                                        (transform
                                         (extract-tags x)
                                         [:a]
                                         (wrap :div)))
                                       [:div]
                                       #(assoc % :tag :td)) } )
           companies)
      ) ))

(defn -main []
  #_(let [{:keys [status headers body error] :as resp} @(http/get "https://angel.co/berlin?page=4")]
      (if error
        (println "Failed, exception: " error)
        (println "HTTP GET success: " body)))
  
  (println 
   (apply str
          (emit*
           {:tag :table :content
            (flatten (map #(do (.println *err* %)
                               (concat (into [] (extract-main-from-file (str % ".json"))))) (range 1 15))) }) 
          )) )
